var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
    name: 'DiRAD API',
    description: 'The service that runs the DiRAD API server using Node.js.',
    script: 'server.js'
});

svc.on('install', function () {
    console.log("Service Installed Successfully!")
    svc.start();
});

svc.on('alreadyinstalled', function () {
    console.log("Service Already Installed")
});
svc.on('invalidinstallation', function () {
    console.log("Service Invalid")
});
svc.on('error', function () {
    console.log("An Error Occurred")
});

svc.install();