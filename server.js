const tedious = require('tedious');//Tedious is a pure-Javascript implementation of the TDS protocol, which is used to interact with instances of Microsoft's SQL Server.
const express = require('express'); //Used to host REST api routes
const bodyParser = require('body-parser'); //Parse JSON/Twilio Data
const fs = require('fs');//Read data from the filesystem

//Express App
const app = express(); //Start Express
app.listen(process.env.PORT || 5000); //Run Express on port 5000 or the azure port
app.use(bodyParser.json()) //Parse JSON
app.use(bodyParser.urlencoded({ //Parse incomming twilio data
    extended: false
}));

var config = JSON.parse(fs.readFileSync('./config.json'))//Read config data from file system


app.post('/', auth, (req, res) => {
    enterSQL(req.body);//Call SQL function
    res.type('application/json')
    res.status(200).end(JSON.stringify("OK"))//Respond to IVR
});//Route for new SQL data

app.get('/heartbeat', (req, res) => {
    res.sendStatus(200);//Route for monitoring tool 
});//Route for monitoring tool 


function enterSQL(data) {
    var connection = new tedious.Connection(config.database);

    connection.on("error", (err) => { console.log(err) })//Log SQL Errors to console
    connection.on("connect", function (err) {//connect to SQL
        if (err) {
            console.log(err);//Log connection error to console
        }
        var result = [];

        var requestString = 'INSERT CALLS_MADE (' +
            'Call_Begin_DateTime, Station_ID,' +
            'Station_Name, Customer_Name, Forecast_System, Forecast_System_ID,' +
            'Call_Group_ID, Reading_Time_Entered,Exit_Reason '

        var counter = 0;
        for (reading of data.readings) {
            ++counter;
            requestString = requestString + ', Tank_' + counter + '_Reading, Tank_' + counter + '_Type '

        }
        requestString = requestString + ')VALUES (' +
            '@Call_Begin_DateTime, @Station_ID,' +
            '@Station_Name, @Customer_Name, @Forecast_System, @Forecast_System_ID,' +
            '@Call_Group_ID, @Reading_Time_Entered,@Exit_Reason '
        var requestCounter = 0;
        for (reading of data.readings) {
            ++requestCounter;
            requestString = requestString + ', @Tank_' + requestCounter + '_Reading, @Tank_' + requestCounter + '_Type '

        }
        requestString = requestString + ');'
        console.log(requestString)

        var request = new tedious.Request(
            requestString,
            function (err, rowCount, rows) {
                if (err) {
                    console.log(err)
                } else {
                    console.log(rowCount + ' row(s) updated');
                }
                connection.close();
            })
        request.addParameter('Call_Begin_DateTime', tedious.TYPES.DateTime, data.callBeginDateTime);
        request.addParameter('Station_ID', tedious.TYPES.VarChar, data.fileData["stationID"]);
        request.addParameter('Station_Name', tedious.TYPES.VarChar, data.fileData["Station Name"]);
        request.addParameter('Customer_Name', tedious.TYPES.VarChar, data.fileData["Customer Name"]);
        request.addParameter('Forecast_System', tedious.TYPES.VarChar, data.fileData["Forecast System"]);
        request.addParameter('Forecast_System_ID', tedious.TYPES.VarChar, data.fileData["Forecast System ID"]);
        request.addParameter('Call_Group_ID', tedious.TYPES.VarChar, data.fileData["Call Group ID"]);
        request.addParameter('Reading_Time_Entered', tedious.TYPES.DateTime, data["readingTimeEntered"]);
        request.addParameter('Exit_Reason', tedious.TYPES.VarChar, data["existReson"]);
        var paramCounter = 0;
        for (reading of data.readings) {
            ++paramCounter;
            request.addParameter('Tank_' + paramCounter + '_Reading', tedious.TYPES.Int, reading.reading);
            request.addParameter('Tank_' + paramCounter + '_Type', tedious.TYPES.VarChar, reading.type);
        }
        request.on("row", function (columns) {
            var item = {};
            columns.forEach(function (column) {
                item[column.metadata.colName] = column.value;
            });
            result.push(item);
        });
        connection.execSql(request)


    })

};//Sends data to msSQL

function auth(req, res, next) {
    var auth = {
        login: config.appUser,
        password: config.appPass
    } //Username and password, can be loaded from json file or DB in the future.

    var b64auth = new Buffer.from((req.headers.authorization || '').split(' ')[
        1] || '', 'base64').toString().split(':') //Grab the authorization header, decode, and split it.
    var login = b64auth.shift() //First array member is username
    var password = b64auth.join('') //The concatenation of remaining items is password. (Passwords can include :)

    if (!login || !password || login !== auth.login || password !== auth.password) { //If either field is missing or incorrect
        res.set('WWW-Authenticate', 'Basic realm="DiRAD Flow"') //Set Basic Auth required headers
        res.status(401).end('Authentication required.') //Send an error back
        return false //Stop processing this request
    }
    next()
};//Parses Basic Auth Data

console.log("");
console.log("██████╗ ██╗██████╗  █████╗ ██████╗     ████████╗███████╗ ██████╗██╗  ██╗");
console.log("██╔══██╗██║██╔══██╗██╔══██╗██╔══██╗    ╚══██╔══╝██╔════╝██╔════╝██║  ██║");
console.log("██║  ██║██║██████╔╝███████║██║  ██║       ██║   █████╗  ██║     ███████║");
console.log("██║  ██║██║██╔══██╗██╔══██║██║  ██║       ██║   ██╔══╝  ██║     ██╔══██║");
console.log("██████╔╝██║██║  ██║██║  ██║██████╔╝       ██║   ███████╗╚██████╗██║  ██║");
console.log("╚═════╝ ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝        ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝");